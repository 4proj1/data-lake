FROM cassandra

COPY docker-entrypoint.sh /docker-entrypoint.sh

COPY docker-entrypoint-initdb.d/init.cql /docker-entrypoint-initdb.d/

RUN chmod a+x docker-entrypoint.sh